/*
 * USB Single-Press Keyboard with
 * DIP switch selection of code
 */

/* C Library Includes */

#include <stdint.h>

/* Arduino Includes */

#include "Keyboard.h"

/* Application Includes */

#include "inputs.h"
#include "keymap.h"

/* Defines */

#define USE_SERIAL

/* Local Variables */

static IO io;

void setup()
{
    #ifdef USE_SERIAL
    Serial.begin(115200);
    #endif

    Keyboard.begin();

    #ifdef USE_SERIAL
    Serial.println("Single USB Keypress Application");
    #endif

    io_setup();
}

void loop() {
    io_loop(io);

    if (io.ctrl)
    {
        Keyboard.press(KEY_LEFT_CTRL);
    }
    else
    {
        Keyboard.release(KEY_LEFT_CTRL);
    }

    if (io.alt)
    {
        Keyboard.press(KEY_LEFT_ALT);
    }
    else
    {
        Keyboard.release(KEY_LEFT_ALT);
    }

    if (io.shift)
    {
        Keyboard.press(KEY_LEFT_SHIFT);
    }
    else
    {
        Keyboard.release(KEY_LEFT_SHIFT);
    }

    if (io.keypress)
    {
        #ifdef USE_SERIAL
        Serial.println("Keypress...");
        #endif

        if (io.dipswitch < 200)
        {
            #ifdef USE_SERIAL
            Serial.print("Dipswitch="); Serial.println(io.dipswitch);
            #endif

            uint16_t scancode = DIPSWITCH_TO_KEYCODE_MAP[io.dipswitch];
            if (scancode != NO_SCANCODE)
            {
                #ifdef USE_SERIAL
                Serial.print("Keycode="); Serial.println(scancode);
                #endif

                Keyboard.press(0xF000 + scancode);
                Keyboard.release(0xF000 + scancode);
            }
            else
            {
                #ifdef USE_SERIAL
                Serial.println("No keycode");
                #endif

            }
        }
        else
        {
            #ifdef USE_SERIAL
            Serial.print("DIP out of range ("); Serial.print(io.dipswitch); Serial.println(")");
            #endif

        }
    }
}