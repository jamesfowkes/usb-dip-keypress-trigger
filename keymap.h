#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

#define NO_SCANCODE 0xFF

// DIP switch value to USB HID scancode value

static const uint16_t DIPSWITCH_TO_KEYCODE_MAP[] = 
{
    NO_SCANCODE,        // 0: NUL
    NO_SCANCODE,        // 1: SOH
    NO_SCANCODE,        // 2: STX
    NO_SCANCODE,        // 3: ETX
    NO_SCANCODE,        // 4: EOT
    NO_SCANCODE,        // 5: ENQ
    NO_SCANCODE,        // 6: ACK
    NO_SCANCODE,        // 7: BEL
    KEY_BACKSPACE,      // 8: BS
    KEY_TAB,            // 9: HT
    KEY_RETURN,         // 10: LF
    NO_SCANCODE,        // 11: VT
    NO_SCANCODE,        // 12: FF
    KEY_RETURN,         // 13: CR
    NO_SCANCODE,        // 14: SO
    NO_SCANCODE,        // 15: SI
    NO_SCANCODE,        // 16: DLE
    NO_SCANCODE,        // 17: DC1
    NO_SCANCODE,        // 18: DC2
    NO_SCANCODE,        // 19: DC3
    NO_SCANCODE,        // 20: DC4
    NO_SCANCODE,        // 21: NAK
    NO_SCANCODE,        // 22: SYN
    NO_SCANCODE,        // 23: ETB
    NO_SCANCODE,        // 24: CAN
    NO_SCANCODE,        // 25: EM
    NO_SCANCODE,        // 26: SUB
    KEY_ESC,            // 27: ESC
    NO_SCANCODE,        // 28: FS
    NO_SCANCODE,        // 29: GS
    NO_SCANCODE,        // 30: RS
    NO_SCANCODE,        // 31: US
    0x2C,               // 32: Space
    0x1F,               // 33: !
    0x20,               // 34: "
    0x32,               // 35: #
    0x22,               // 36: $
    0x23,               // 37: %
    0x25,               // 38: &
    0x34,               // 39: '
    0x27,               // 40: (
    0x1E,               // 41: )
    0x26,               // 42: *
    0x2E,               // 43: +
    0x36,               // 44: ,
    0x2D,               // 45: -
    0x37,               // 46: .
    0x38,               // 47: /
    0x1E,               // 48: 0
    0x1F,               // 49: 1
    0x20,               // 50: 2
    0x21,               // 51: 3
    0x22,               // 52: 4
    0x23,               // 53: 5
    0x24,               // 54: 6
    0x25,               // 55: 7
    0x26,               // 56: 8
    0x27,               // 57: 9
    0x33,               // 58: :
    0x33,               // 59: ;
    0x36,               // 60: <
    0x2E,               // 61: =
    0x37,               // 62: >
    0x38,               // 63: ?
    0x34,               // 64: @
    0x04,               // 65: A
    0x05,               // 66: B
    0x06,               // 67: C
    0x07,               // 68: D
    0x08,               // 69: E
    0x09,               // 70: F
    0x0A,               // 71: G
    0x0B,               // 72: H
    0x0C,               // 73: I
    0x0D,               // 74: J
    0x0E,               // 75: K
    0x0F,               // 76: L
    0x10,               // 77: M
    0x11,               // 78: N
    0x12,               // 79: O
    0x13,               // 80: P
    0x14,               // 81: Q
    0x15,               // 82: R
    0x16,               // 83: S
    0x17,               // 84: T
    0x18,               // 85: U
    0x19,               // 86: V
    0x1A,               // 87: W
    0x1B,               // 88: X
    0x1C,               // 89: Y
    0x1D,               // 90: Z
    0x2F,               // 91: [
    0x31,               // 92: Backslash
    0x30,               // 93: ]
    0x24,               // 94: ^
    0x2D,               // 95: _
    0x35,               // 96: `
    0x04,               // 97: a
    0x05,               // 98: b
    0x06,               // 99: c
    0x07,               // 100: d
    0x08,               // 101: e
    0x09,               // 102: f
    0x0A,               // 103: g
    0x0B,               // 104: h
    0x0C,               // 105: i
    0x0D,               // 106: j
    0x0E,               // 107: k
    0x0F,               // 108: l
    0x10,               // 109: m
    0x11,               // 110: n
    0x12,               // 111: o
    0x13,               // 112: p
    0x14,               // 113: q
    0x15,               // 114: r
    0x16,               // 115: s
    0x17,               // 116: t
    0x18,               // 117: u
    0x19,               // 118: v
    0x1A,               // 119: w
    0x1B,               // 120: x
    0x1C,               // 121: y
    0x1D,               // 122: z
    0x2F,               // 123: {
    0x31,               // 124: |
    0x30,               // 125: }
    0x32,               // 126: ~
    KEY_DELETE,         // 127: DEL
    NO_SCANCODE,        // 128
    KEY_F1,             // 129
    KEY_F2,             // 130
    KEY_F3,             // 131
    KEY_F4,             // 132
    KEY_F5,             // 133
    KEY_F6,             // 134
    KEY_F7,             // 135
    KEY_F8,             // 136
    KEY_F9,             // 137
    KEY_F10,            // 138
    KEY_F11,            // 139
    KEY_F12,            // 140
    KEY_RIGHT_ARROW,    // 141
    KEY_LEFT_ARROW,     // 142
    KEY_DOWN_ARROW,     // 143
    KEY_UP_ARROW,       // 144
    KEY_PAGE_UP,        // 145
    KEY_PAGE_DOWN,      // 146
    KEY_END,            // 147
    KEY_INSERT,         // 148
    KEY_HOME,           // 149
    KEY_PAGE_DOWN,      // 150
    NO_SCANCODE,        // 151
    NO_SCANCODE,        // 152
    NO_SCANCODE,        // 153
    NO_SCANCODE,        // 154
    NO_SCANCODE,        // 155
    NO_SCANCODE,        // 156
    NO_SCANCODE,        // 157
    NO_SCANCODE,        // 158
    NO_SCANCODE,        // 159
    NO_SCANCODE,        // 160
    NO_SCANCODE,        // 161
    NO_SCANCODE,        // 162
    NO_SCANCODE,        // 163
    NO_SCANCODE,        // 164
    NO_SCANCODE,        // 165
    NO_SCANCODE,        // 166
    NO_SCANCODE,        // 167
    NO_SCANCODE,        // 168
    NO_SCANCODE,        // 169
    NO_SCANCODE,        // 170
    NO_SCANCODE,        // 171
    NO_SCANCODE,        // 172
    NO_SCANCODE,        // 173
    NO_SCANCODE,        // 174
    NO_SCANCODE,        // 175
    NO_SCANCODE,        // 176
    NO_SCANCODE,        // 177
    NO_SCANCODE,        // 178
    NO_SCANCODE,        // 179
    NO_SCANCODE,        // 180
    NO_SCANCODE,        // 181
    NO_SCANCODE,        // 182
    NO_SCANCODE,        // 183
    NO_SCANCODE,        // 184
    NO_SCANCODE,        // 185
    NO_SCANCODE,        // 186
    NO_SCANCODE,        // 187
    NO_SCANCODE,        // 188
    NO_SCANCODE,        // 189
    NO_SCANCODE,        // 190
    NO_SCANCODE,        // 191
    KEY_LEFT_CTRL,     // 192
    KEY_LEFT_SHIFT,    // 193
    KEY_LEFT_ALT,      // 194
    KEY_LEFT_GUI,      // 195
    KEY_RIGHT_CTRL,    // 196
    KEY_RIGHT_SHIFT,   // 197
    KEY_RIGHT_ALT,     // 198
    KEY_RIGHT_GUI,     // 199
    KEY_CAPS_LOCK      // 200
    // No codes past 200
};

#endif