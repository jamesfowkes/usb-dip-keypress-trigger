/* C Library Includes */

#include <stdint.h>

/* Arduino Includes */

#include "Bounce2.h"

/* Application Includes */

#include "inputs.h"

/* Defines, constants, typedefs */

#define KEYPRESS_PIN A0

#define DIP0 12
#define DIP1 11
#define DIP2 10
#define DIP3 9
#define DIP4 8
#define DIP5 7
#define DIP6 6
#define DIP7 5

#define CTRL_MODIFIER_PIN 2
#define ALT_MODIFIER_PIN 3
#define SHIFT_MODIFIER_PIN 4

static const uint8_t DIP_PINS[] = {DIP0, DIP1, DIP2, DIP3, DIP4, DIP5, DIP6, DIP7};

/* Local Data */

static Bounce keypress_input = Bounce();

static Bounce dip_inputs[] = {
    Bounce(),
    Bounce(),
    Bounce(),
    Bounce(),
    Bounce(),
    Bounce(),
    Bounce(),
    Bounce()
};

static Bounce ctrl_modifier_pin = Bounce();
static Bounce alt_modifier_pin = Bounce();
static Bounce shift_modifier_pin = Bounce();

/* Public Functions */

void io_setup(void)
{

    pinMode(KEYPRESS_PIN, INPUT_PULLUP);
    keypress_input.attach(KEYPRESS_PIN);
    keypress_input.interval(10);

    for (uint8_t i = 0; i<8; i++)
    {
        pinMode(DIP_PINS[i], INPUT_PULLUP);
        dip_inputs[i].attach(DIP_PINS[i]);
        dip_inputs[i].interval(10);
    }

    pinMode(CTRL_MODIFIER_PIN, INPUT_PULLUP);
    ctrl_modifier_pin.attach(CTRL_MODIFIER_PIN);
    ctrl_modifier_pin.interval(10);

    pinMode(ALT_MODIFIER_PIN, INPUT_PULLUP);
    alt_modifier_pin.attach(ALT_MODIFIER_PIN);
    alt_modifier_pin.interval(10);

    pinMode(SHIFT_MODIFIER_PIN, INPUT_PULLUP);
    shift_modifier_pin.attach(SHIFT_MODIFIER_PIN);
    shift_modifier_pin.interval(10);
}

void io_loop(IO& io)
{
    keypress_input.update();
    io.keypress = keypress_input.fallingEdge();

    io.dipswitch = 0;
    for (uint8_t i = 0; i<8; i++)
    {
        dip_inputs[i].update();
        io.dipswitch |= dip_inputs[i].read() ? 0 : (1 << i);
    }

    ctrl_modifier_pin.update();
    io.ctrl = !ctrl_modifier_pin.read();

    alt_modifier_pin.update();
    io.alt = !alt_modifier_pin.read();

    shift_modifier_pin.update();
    io.shift = !shift_modifier_pin.read();
}
