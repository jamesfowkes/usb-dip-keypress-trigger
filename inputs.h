#ifndef _INPUTS_H_
#define _INPUTS_H_

typedef struct io
{
    bool keypress;
    uint8_t dipswitch;
    bool ctrl;
    bool alt;
    bool shift;
} IO;

void io_setup(void);
void io_loop(IO& io);

#endif
